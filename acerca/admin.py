from django import forms
from django.db import models
from django.contrib import admin

from .models import Informacion, Contacto

@admin.register(Informacion)
class InformacionAdmin(admin.ModelAdmin):
	list_display = ('nombre', 'cuerpo',)
	search_fields = ('nombre',)
	list_display_links = ('nombre', 'cuerpo',)

	formfield_overrides = { models.TextField: {'widget': forms.Textarea(attrs={'class':'ckeditor'})}, }
	class Media:
		js = ('ckeditor/ckeditor.js',)

@admin.register(Contacto)
class ContactoAdmin(admin.ModelAdmin):
	list_display = ('telefono', 'celular', 'email',)
	list_display_links = ('telefono', 'celular', 'email',)
