from django.db import models
from django.core.exceptions import ValidationError


class Informacion(models.Model):
	nombre = models.CharField(max_length=100, unique=True)
	cuerpo = models.TextField()

	def __unicode__(self):
		return self.nombre

	class Meta:
		verbose_name_plural = 'Informacion'

class Contacto(models.Model):
	direccion = models.CharField(max_length=300)
	telefono = models.CharField(max_length=12)
	celular = models.CharField(max_length=12)
	email = models.EmailField(blank=True)

	def __unicode__(self):
		return self.telefono

	def validar_contacto(self):
		if Contacto.objects.all().count() > 0:
			if self.pk is None:
				return False
		return True

	def clean(self):
		if not self.validar_contacto():
			raise ValidationError('Solo se permite un contacto, porfavor modifique el existente')