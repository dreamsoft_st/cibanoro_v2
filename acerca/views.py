from django.shortcuts import render

from .models import Informacion, Contacto
from noticia.models import Noticia

def informacion(request, pagina):
	acerca = Informacion.objects.filter(nombre__iexact=pagina)
	if acerca.count() > 0:
		acerca = acerca[0]
	last = Noticia.objects.order_by('-id')[:15]

	return render(request, 'acerca.html', {'informacion': acerca, 'lasts':last})

def contacto(request):
	c = Contacto.objects.order_by('-id').all()[:1]
	c = c.first()
	return render(request, 'contacto.html', {'contacto': c})