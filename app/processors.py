import random
from django.template import RequestContext
from noticia.models import Seccion
from equipo.models import Equipo, Juego
from publicidad.models import Banner

from datetime import datetime

def menu(request):
	seccion = Seccion.objects.order_by('nombre')
	today = datetime.today()
	publicidad = Banner.objects.filter(fecha_expira__gte=today).order_by('?')[:4]
	juegos_menu_hoy = Juego.objects.filter(fecha=today)[:4]
	cant = 4 - juegos_menu_hoy.count()
	juegos_menu = Juego.objects.filter(fecha__gte=today)[:cant]
	juegos_pasados = Juego.objects.filter(fecha__lte=today)
	varonil_team = Equipo.objects.filter(liga='varonil')[:10]
	femenil_team = Equipo.objects.filter(liga='femenil')[:10]
	barra = False
	if len(varonil_team) > 0 and len(femenil_team) > 0:
		barra = True

	datos = {
		'secciones_menu': seccion,
		'publicidad': publicidad,
		'juegos_menu_hoy': juegos_menu_hoy,
		'juegos_menu': juegos_menu,
		'juegos_pasados': juegos_pasados,
		'varonil_team': varonil_team,
		'femenil_team': femenil_team,
		'barra': barra
	}
	return  datos