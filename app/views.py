from django.shortcuts import render
from django.db.models import Q, F, Avg, Count

from noticia.models import Seccion, Noticia
from blog.models import Tag, Articulo
from equipo.models import Juego, Temporada, Estadistica

from datetime import datetime


bandera = False
def home(request):

	global bandera
	weekday = datetime.now().weekday()
	if weekday == 0 and bandera == False:
		Noticia.objects.all().update(visto = 0)
		bandera = True
	elif weekday != 0 and bandera == True:
		bandera = False

	last_temp = Temporada.objects.order_by('-id').first()
	today = datetime.today()
	noticias_dest = Noticia.objects.order_by('-visto', '-fecha')[:8]
	noticias = Noticia.objects.order_by('-id')[:15]
	articulos = Articulo.objects.order_by('-id')[:4]
	juegos_pasados = Juego.objects.filter(fecha__lte=today)

	j_puntos = Estadistica.objects.values('usuario__nombre', 'usuario__apellidos', 'usuario__foto', 'equipo__nombre'). \
		filter(juego__temporada=last_temp).annotate(pts=Avg('puntos'), dcount=Count('usuario')).order_by('-pts')[:3]

	j_asis = Estadistica.objects.values('usuario__nombre', 'usuario__apellidos', 'usuario__foto', 'equipo__nombre'). \
		filter(juego__temporada=last_temp).	annotate(pts=Avg('asistencias'), dcount=Count('usuario')).order_by('-pts')[:3]

	j_rebotes = Estadistica.objects.values('usuario__nombre', 'usuario__apellidos', 'usuario__foto', 'equipo__nombre'). \
		filter(juego__temporada=last_temp).annotate(pts=Avg('rebotes'), dcount=Count('usuario')).order_by('-pts')[:3]
	print j_puntos
	data = {
		'noticias_dest': noticias_dest,
		'noticias': noticias,
		'articulos': articulos,
		'juegos_pasados': juegos_pasados,
		'j_puntos': j_puntos,
		'j_asis': j_asis,
		'j_rebotes': j_rebotes
	}
	return render(request, 'index.html', data)

