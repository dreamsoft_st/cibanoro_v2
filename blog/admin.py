from django import forms
from django.db import models
from django.contrib import admin
from .models import Articulo, Tag

@admin.register(Articulo)
class ArticuloAdmin(admin.ModelAdmin):
	list_display=('image_img','autor','titulo','fecha')
	search_fields = ('titulo',)
	list_display_links = ('titulo', 'autor',)
	raw_id_fields = ('autor',)
	filter_horizontal = ['tags']

	def save_model(self, request, obj, form, change):
		if not change:
			obj.autor = request.user
		obj.save()
	
	formfield_overrides = { models.TextField: {'widget': forms.Textarea(attrs={'class':'ckeditor'})}, }
	class Media:
		js = ('ckeditor/ckeditor.js',)

	def image_img(self, obj):
		return '<img src="/media/%s" width="100">' % obj.imagen
	image_img.short_description = 'Imagen'
	image_img.allow_tags = True

@admin.register(Tag)
class TagAdmin(admin.ModelAdmin):
	list_display = ('nombre',)
	search_fields = ('nombre',)