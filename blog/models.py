from django.db import models
from usuario.models import Usuario
from datetime import datetime
from django.template.defaultfilters import slugify

class Tag(models.Model):
	nombre = models.CharField(max_length=100, unique=True)
	slug = models.SlugField(max_length=60, blank=True, editable=False)

	def __unicode__(self):
		return self.nombre

	def save(self, *args, **kwargs):
		self.slug = slugify(self.nombre)
		super(Tag, self).save(*args, **kwargs)

class Articulo(models.Model):
	autor = models.ForeignKey(Usuario, editable=False)
	titulo = models.CharField(max_length=100, unique=True)
	cuerpo = models.TextField()
	imagen = models.ImageField(upload_to='blog', blank=True, null=True)
	fecha = models.DateTimeField(default=datetime.now)
	tags = models.ManyToManyField(Tag)
	slug = models.SlugField(max_length=60, blank=True, editable=False)

	def __unicode__(self):
		return self.titulo

	def save(self, *args, **kwargs):
		self.slug = slugify(self.titulo)
		super(Articulo, self).save(*args, **kwargs)