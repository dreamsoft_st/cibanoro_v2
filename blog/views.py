from django.shortcuts import render, get_object_or_404
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from .models import Tag, Articulo

def articulo_view(request, slug):
	articulo = get_object_or_404(Articulo, slug=slug)
	tags = Tag.objects.order_by('?')[:8]
	return render(request, 'blog.html', {'articulo': articulo, 'tags': tags})

def tag(request, tag):
	page = request.GET.get('page', 1)
	arts = Articulo.objects.filter(tags__slug=tag)
	paginator = Paginator(arts, 5)
	tags = Tag.objects.order_by('?')[:8]
	try:
		articulos = paginator.page(page)
	except PageNotAnInteger:
		articulos = paginator.page(1)
	except EmptyPage:
		articulos = paginator.page(paginator.num_pages)
	return render(request, 'buscar_blog.html', {'articulos': articulos, 'tags': tags})

def buscar(request):
	q = request.GET.get('q', '')
	page = request.GET.get('page', 1)
	arts = Articulo.objects.filter(titulo__icontains=q)
	paginator = Paginator(arts, 5)
	tags = Tag.objects.order_by('?')[:8]
	try:
		articulos = paginator.page(page)
	except PageNotAnInteger:
		articulos = paginator.page(1)
	except EmptyPage:
		articulos = paginator.page(paginator.num_pages)
	return render(request, 'buscar_blog.html', {'articulos': articulos, 'tags': tags})

def blogs(request):
	page = request.GET.get('page', 1)
	arts = Articulo.objects.order_by('-id')
	paginator = Paginator(arts, 5)
	tags = Tag.objects.order_by('?')[:8]
	try:
		articulos = paginator.page(page)
	except PageNotAnInteger:
		articulos = paginator.page(1)
	except EmptyPage:
		articulos = paginator.page(paginator.num_pages)
	return render(request, 'buscar_blog.html', {'articulos': articulos, 'tags': tags})