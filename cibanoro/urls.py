from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.conf import settings

urlpatterns = patterns('',
	url(r'^$', 'app.views.home', name='home'),
	
	url(r'^blog/buscar/', 'blog.views.buscar', name='buscar'),
	url(r'^blog/tag/([-\w]+)/', 'blog.views.tag', name='tag'),
	url(r'^blog/show/', 'blog.views.blogs', name='blogs'),
	url(r'^blog/([-\w]+)/$', 'blog.views.articulo_view', name='articulo_view'),

	url(r'^noticia/buscar/', 'noticia.views.buscar', name='buscar_noticia'),
	url(r'^noticia/seccion/([-\w]+)/', 'noticia.views.seccion', name='tag_noticia'),
	url(r'^noticia/show/', 'noticia.views.blogs', name='blogs_noticia'),
	url(r'^noticia/([-\w]+)/$', 'noticia.views.articulo_view', name='articulo_view_noticia'),

	url(r'^acerca/contacto/', 'acerca.views.contacto', name='contacto'),
	url(r'^acerca/([-\w]+)/', 'acerca.views.informacion', name='acerca'),

	url(r'^equipo/liga/([-\w]+)/', 'equipo.views.show', name='show'),
	url(r'^equipo/([-\w]+)/$', 'equipo.views.equipo_view', name='equipo_view'),
	url(r'^equipo/([-\w]+)/jugador/([-\w]+)/$', 'equipo.views.jugador_view', name='jugador_view'),
	url(r'^calendario/([-\w]+)/$', 'equipo.views.calendario', name='calendario'),

    url(r'^admin/', include(admin.site.urls)),
)

if settings.DEBUG:
	urlpatterns += patterns('',
		(r'^media/(?P<path>.*)$', 'django.views.static.serve', {
		'document_root': settings.MEDIA_ROOT}))