from django import forms
from django.db import models
from django.contrib import admin

from .models import Equipo, Juego, Estadistica, Temporada, Jugador

@admin.register(Temporada)
class TemporadaAdmin(admin.ModelAdmin):
	list_display = ('nombre',)
	search_filter = ('nombre',)

@admin.register(Jugador)
class JugadorAdmin(admin.ModelAdmin):
	list_display = ('nombre', 'apellidos', 'email', 'rol', 'equipo',)
	list_display_links = ('nombre', 'apellidos', 'email', 'rol', 'equipo',)
	search_filter = ('nombre', 'apellidos',)
	raw_id_fields = ('equipo',)

@admin.register(Equipo)
class EquipoAdmin(admin.ModelAdmin):
	list_display = ('nombre', 'Logo', 'estadio', 'liga',)
	search_filter = ('nombre', )
	list_display_links = ('nombre',)

	formfield_overrides = { models.TextField: {'widget': forms.Textarea(attrs={'class':'ckeditor'})}, }
	class Media:
		js = ('ckeditor/ckeditor.js',)


	def Logo(self, obj):
		return '<img src="/media/%s" width="100">' % obj.logo
	Logo.short_description = 'Logo'
	Logo.allow_tags = True

@admin.register(Juego)
class JuegoAdmin(admin.ModelAdmin):
	list_display = ('fecha', 'equipo_a', 'equipo_b', 'lugar',)
	search_filter = ('equipo_a__nombre', 'equipo_b__nombre', 'lugar',)
	list_display_links = ('fecha', 'equipo_a', 'equipo_b', 'lugar',)
	raw_id_fields = ('equipo_a', 'equipo_b', 'temporada',)

	formfield_overrides = { models.TextField: {'widget': forms.Textarea(attrs={'class':'ckeditor'})}, }
	class Media:
		js = ('ckeditor/ckeditor.js',)

@admin.register(Estadistica)
class EstadisticaAdmin(admin.ModelAdmin):
	pass