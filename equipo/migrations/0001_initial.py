# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Equipo',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nombre', models.CharField(unique=True, max_length=100)),
                ('logo', models.ImageField(upload_to=b'equipos', blank=True)),
                ('estadio', models.CharField(max_length=100, blank=True)),
                ('nombre_corto', models.CharField(max_length=10, blank=True)),
                ('liga', models.CharField(default=b'varonil', max_length=20, choices=[(b'varonil', b'Varonil'), (b'femenil', b'Femenil')])),
                ('procedencia', models.CharField(max_length=100, blank=True)),
                ('slug', models.SlugField(max_length=60, editable=False, blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Estadistica',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('puntos', models.PositiveIntegerField(default=0)),
                ('faltas', models.PositiveIntegerField(default=0)),
                ('rebotes', models.PositiveIntegerField(default=0)),
                ('asistencias', models.PositiveIntegerField(default=0)),
                ('bloqueos', models.PositiveIntegerField(default=0)),
                ('equipo', models.ForeignKey(editable=False, to='equipo.Equipo', null=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Juego',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('fecha', models.DateTimeField()),
                ('anotador', models.CharField(max_length=100, editable=False, blank=True)),
                ('arbitro_a', models.CharField(max_length=100, editable=False, blank=True)),
                ('arbitro_b', models.CharField(max_length=100, editable=False, blank=True)),
                ('puntaje_equipo_a', models.IntegerField(null=True, editable=False)),
                ('puntaje_equipo_b', models.IntegerField(null=True, editable=False)),
                ('lugar', models.CharField(max_length=200)),
                ('equipo_a', models.ForeignKey(related_name='equipo_a', to='equipo.Equipo')),
                ('equipo_b', models.ForeignKey(related_name='equipo_b', to='equipo.Equipo')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Jugador',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nombre', models.CharField(max_length=100)),
                ('apellidos', models.CharField(max_length=200)),
                ('email', models.EmailField(unique=True, max_length=75, blank=True)),
                ('foto', models.ImageField(null=True, upload_to=b'jugadores')),
                ('fecha_nacimiento', models.DateField(null=True)),
                ('rol', models.CharField(default=b'jugador', max_length=20, blank=True, choices=[(b'jugador', b'JUGADOR'), (b'entrenador', b'ENTRENADOR'), (b'directivo', b'DIRECTIVO')])),
                ('procedencia', models.CharField(max_length=200, blank=True)),
                ('no_jersey', models.IntegerField(null=True, blank=True)),
                ('posicion', models.CharField(default=b'base', max_length=100, choices=[(b'base', b'Base'), (b'escolta', b'Escolta'), (b'alero', b'Alero'), (b'ala-pivot', b'Ala-pivot'), (b'pivot', b'Pivot')])),
                ('equipo', models.ForeignKey(to='equipo.Equipo')),
            ],
            options={
                'verbose_name_plural': 'Jugadores',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Temporada',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nombre', models.CharField(unique=True, max_length=100)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='juego',
            name='temporada',
            field=models.ForeignKey(to='equipo.Temporada'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='estadistica',
            name='juego',
            field=models.ForeignKey(to='equipo.Juego'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='estadistica',
            name='usuario',
            field=models.ForeignKey(to='equipo.Jugador'),
            preserve_default=True,
        ),
    ]
