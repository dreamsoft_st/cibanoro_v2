# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('equipo', '0002_auto_20150720_1235'),
    ]

    operations = [
        migrations.AddField(
            model_name='equipo',
            name='url',
            field=models.URLField(blank=True),
        ),
    ]
