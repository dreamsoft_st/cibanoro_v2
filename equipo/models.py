from django.db import models
from django.db.models import Q, F, Avg, Count
from django.template.defaultfilters import slugify
from django.core.exceptions import ValidationError

from datetime import date

class Temporada(models.Model):
	nombre = models.CharField(max_length=100, unique=True)

	def __unicode__(self):
		return self.nombre

class Equipo(models.Model):
	nombre = models.CharField(max_length=100, unique=True)
	logo = models.ImageField(upload_to='equipos', blank=True)
	estadio = models.CharField(max_length=100, blank=True)
	nombre_corto = models.CharField(max_length=10, blank=True)
	SEXO = (
		('varonil', 'Varonil'),
		('femenil', 'Femenil'),
	)
	liga = models.CharField(max_length=20, choices=SEXO, default='varonil')
	procedencia = models.CharField(max_length=100, blank=True)
	slug = models.SlugField(max_length=60, blank=True, editable=False)
	url = models.URLField(blank=True)

	def __unicode__(self):
		return self.nombre

	def save(self, *args, **kwargs):
		self.slug = slugify(self.nombre)
		super(Equipo, self).save(*args, **kwargs)

	def get_couch(self):
		p = Jugador.objects.filter(equipo=self.pk, rol='entrenador').first()
		if p:
			return p
		return ''
	def get_directivo(self):
		p = Jugador.objects.filter(equipo=self.pk, rol='directivo').first()
		if p is not None:
			return p
		return ''
	def get_players(self):
		return Jugador.objects.filter(equipo=self.pk, rol='jugador')
	def get_games_count(self):
		return Juego.objects.filter(
			Q(equipo_a=self.pk) | Q(equipo_b=self.pk)
		).count()
	def get_wins_count(self):
		return Juego.objects.filter(
			(Q(equipo_a=self.pk) | Q(equipo_b=self.pk)), puntaje_equipo_a__gt=F('puntaje_equipo_b')
		).count()
	def get_losses_count(self):
		return Juego.objects.filter(
			(Q(equipo_a=self.pk) | Q(equipo_b=self.pk)), puntaje_equipo_a__lt=F('puntaje_equipo_b')
		).count()
	def get_percent(self):
		total = self.get_games_count()
		wins = self.get_wins_count()
		if total == 0:
			return '0%'	
		return '{}%'.format( (100*float(wins)/float(total)) )


class Jugador(models.Model):
	nombre = models.CharField(max_length=100)
	apellidos = models.CharField(max_length=200)
	email = models.EmailField(blank=True, unique=True)
	foto = models.ImageField(upload_to='jugadores', null=True)
	fecha_nacimiento = models.DateField(null=True)
	ROLES = (
		('jugador', 'JUGADOR'),
		('entrenador', 'ENTRENADOR'),
		('directivo', 'DIRECTIVO')
	)
	rol = models.CharField(max_length=20, choices=ROLES, default='jugador', blank=True)
	procedencia = models.CharField(max_length=200,blank=True)
	no_jersey = models.IntegerField(null=True, blank=True)
	POSICION = (
		('base', 'Base'),
		('escolta', 'Escolta'),
		('alero', 'Alero'),
		('ala-pivot', 'Ala-pivot'),
		('pivot', 'Pivot'),
	)
	posicion = models.CharField(max_length=100, choices=POSICION, default='base')
	equipo = models.ForeignKey(Equipo)

	def __unicode__(self):
		return self.nombre

	def get_promedios(self):
		last_temp = Temporada.objects.order_by('-id').first()
		res = Estadistica.objects.filter(usuario=self.pk, juego__temporada=last_temp). \
			aggregate(Avg('puntos'), Avg('asistencias'), Avg('rebotes'), Avg('bloqueos'), Count('rebotes'))
		if res['puntos__avg'] is None:
			return {'puntos__avg': 0, 'asistencias__avg': 0, 'rebotes__avg':0, 'rebotes__count': 0, 'bloqueos__avg': 0}
		return res
	def get_promedios_temp(self):
		last_temp = Temporada.objects.order_by('-id')
		res = []
		for temp in last_temp:
			e = Estadistica.objects.filter(usuario=self.pk, juego__temporada=temp). \
				annotate(puntoss=Avg('puntos'), asistenciass=Avg('asistencias'), rebotess=Avg('rebotes'), bloqueoss=Avg('bloqueos'), total=Count('rebotes'))
			e = e.first()
			if e is not None:
				res.append(e)

		return res

	def get_age(self):
		born = self.fecha_nacimiento
		if born is None:
			return '--'
		today = date.today()
		try: 
			birthday = born.replace(year=today.year)
		except ValueError:
			birthday = born.replace(year=today.year, month=born.month+1, day=1)
		if birthday > today:
			return today.year - born.year - 1
		else:
			return today.year - born.year

	class Meta:
		verbose_name_plural = 'Jugadores'


class Juego(models.Model):
	fecha = models.DateTimeField()
	equipo_a = models.ForeignKey(Equipo, related_name='equipo_a')
	equipo_b = models.ForeignKey(Equipo, related_name='equipo_b')
	anotador = models.CharField(max_length=100, blank=True, editable=True)
	arbitro_a = models.CharField(max_length=100, blank=True, editable=True)
	arbitro_b = models.CharField(max_length=100, blank=True, editable=True)
	puntaje_equipo_a = models.IntegerField(null=True, blank=True, editable=True)
	puntaje_equipo_b = models.IntegerField(null=True, blank=True, editable=True)
	lugar = models.CharField(max_length=200)
	temporada = models.ForeignKey(Temporada)

	def __unicode__(self):
		return "%s vs %s - %s" % (self.equipo_a.nombre, self.equipo_b.nombre, self.fecha)

	def validar_juego(self):
		if self.equipo_a == self.equipo_b:
			return False
		return True
	def validar_equipos(self):
		if self.equipo_a.liga != self.equipo_b.liga:
			return False
		return True
	def clean(self):
		if not self.validar_juego():
			raise ValidationError('Los equipos seleccionados son el mismo')
		elif not self.validar_equipos():
			raise ValidationError('Los equipos son de diferentes ligas')

class Estadistica(models.Model):
	usuario = models.ForeignKey(Jugador)
	juego = models.ForeignKey(Juego)
	equipo = models.ForeignKey(Equipo, editable=True, null=True)
	puntos = models.PositiveIntegerField(default=0)
	faltas = models.PositiveIntegerField(default=0)
	rebotes = models.PositiveIntegerField(default=0)
	asistencias = models.PositiveIntegerField(default=0)
	bloqueos = models.PositiveIntegerField(default=0)

	def __unicode__(self):
		return self.usuario.nombre

	def save(self, *args, **kwargs):
		self.equipo = self.usuario.equipo
		super(Estadistica, self).save(*args, **kwargs)