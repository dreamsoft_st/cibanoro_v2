from django.shortcuts import render, get_object_or_404
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from .models import Equipo, Juego, Temporada, Jugador

def show(request, liga):
	equips = Equipo.objects.filter(liga=liga).all()
	page = request.GET.get('page', 1)
	paginator = Paginator(equips, 4)
	try:
		equipos = paginator.page(page)
	except PageNotAnInteger:
		equipos = paginator.page(1)
	except EmptyPage:
		equipos = paginator.page(paginator.num_pages)


	data = {
		'equipos': equipos,
		'liga': liga
	}
	return render(request, 'equipos.html', data)

def equipo_view(request, slug):
	equipo = get_object_or_404(Equipo, slug=slug)
	return render(request, 'equipo.html', {'equipo': equipo})

def jugador_view(request, slug, jugador_id):
	jugador = get_object_or_404(Jugador, pk=jugador_id)
	return render(request, 'jugador.html', {'jugador': jugador})

def calendario(request, liga):
	temp = request.GET.get('temporada', '')
	temporadas = Temporada.objects.order_by('-id')
	if len(temp) > 0:
		juegos = Juego.objects.filter(temporada__nombre=temp).order_by('fecha').all()
	else:
		juegos = Juego.objects.filter(temporada=temporadas.first()).order_by('fecha').all()

	print juegos
	data = {
		'juegos': juegos,
		'temporadas': temporadas,
		'liga': liga,
		'tempo': temp
	}
	return render(request, 'calendario.html', data)