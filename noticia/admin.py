from django import forms
from django.db import models
from django.contrib import admin
from .models import Noticia, Seccion, NoticiaImagen

class NoticiaInline(admin.StackedInline):
	model = NoticiaImagen
	extra = 3
	verbose_name_plural = 'Imagenes'

@admin.register(Noticia)
class NoticiaAdmin(admin.ModelAdmin):
	list_display=('image_img','autor','titulo','fecha')
	search_fields = ('titulo',)
	list_display_links = ('titulo', 'autor',)
	raw_id_fields = ('autor', 'seccion')
	inlines = [ NoticiaInline, ]

	def save_model(self, request, obj, form, change):
		if not change:
			obj.autor = request.user
		obj.save()
	
	formfield_overrides = { models.TextField: {'widget': forms.Textarea(attrs={'class':'ckeditor'})}, }
	class Media:
		js = ('ckeditor/ckeditor.js',)

	def image_img(self, obj):
		return '<img src="/media/%s" width="100">' % obj.imagen
	image_img.short_description = 'Imagen'
	image_img.allow_tags = True

@admin.register(Seccion)
class SeccionAdmin(admin.ModelAdmin):
	list_display = ('nombre',)
	search_fields = ('nombre',)