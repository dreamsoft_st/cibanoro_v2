# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Noticia',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('titulo', models.CharField(unique=True, max_length=200)),
                ('descripcion', models.CharField(max_length=200)),
                ('cuerpo', models.TextField()),
                ('imagen', models.ImageField(upload_to=b'noticias')),
                ('fecha', models.DateTimeField(default=datetime.datetime.now)),
                ('visto', models.PositiveIntegerField(default=0, editable=False)),
                ('slug', models.SlugField(max_length=60, editable=False, blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Seccion',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nombre', models.CharField(unique=True, max_length=100)),
                ('menu_principal', models.BooleanField(default=False)),
                ('slug', models.SlugField(max_length=60, editable=False, blank=True)),
            ],
            options={
                'verbose_name_plural': 'Secciones',
            },
            bases=(models.Model,),
        ),
    ]
