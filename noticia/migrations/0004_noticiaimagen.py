# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('noticia', '0003_remove_seccion_menu_principal'),
    ]

    operations = [
        migrations.CreateModel(
            name='NoticiaImagen',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('imagen', models.ImageField(upload_to=b'noticias_imagen')),
                ('noticia', models.ForeignKey(to='noticia.Noticia')),
            ],
        ),
    ]
