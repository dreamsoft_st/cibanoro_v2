from django.db import models
from usuario.models import Usuario
from datetime import datetime
from django.template.defaultfilters import slugify

class Seccion(models.Model):
	nombre = models.CharField(max_length=100, unique=True)
	slug = models.SlugField(max_length=60, blank=True, editable=False)

	def __unicode__(self):
		return self.nombre
		
	class Meta:
		verbose_name_plural = 'Secciones'

	def save(self, *args, **kwargs):
		self.slug = slugify(self.nombre)
		super(Seccion, self).save(*args, **kwargs)

class Noticia(models.Model):
	autor = models.ForeignKey(Usuario, editable=False)
	titulo = models.CharField(max_length=200, unique=True)
	descripcion = models.CharField(max_length=200)
	cuerpo = models.TextField()
	imagen = models.ImageField(upload_to='noticias')
	fecha = models.DateTimeField(default=datetime.now)
	seccion = models.ForeignKey(Seccion)
	visto = models.PositiveIntegerField(default=0, editable=False)
	slug = models.SlugField(max_length=60, blank=True, editable=False)

	def __unicode__(self):
		return self.titulo

	def imagenes(self):
		return NoticiaImagen.objects.filter(noticia=self.pk)

	def save(self, *args, **kwargs):
		try:
			this = Noticia.objects.get(pk=self.pk)
			if this.imagen != self.imagen:
				this.imagen.delete(save=False)
		except:
			pass
		super(Noticia, self).save(*args, **kwargs)
		if not self.slug:
			self.slug = slugify(self.titulo[:50]+str(self.id))
			self.save()

class NoticiaImagen(models.Model):
	imagen = models.ImageField(upload_to='noticias_imagen')
	noticia = models.ForeignKey(Noticia)

	class meta:
		verbose_name_plural = 'Imagenes'
	def save(self, *args, **kwargs):
		try:
			this = NoticiaImagen.objects.get(pk=self.pk)
			if this.imagen != self.imagen:
				this.imagen.delete(save=False)
		except:
			pass
		super(NoticiaImagen, self).save(*args, **kwargs)