from django.shortcuts import render, get_object_or_404
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from .models import Seccion, Noticia

def articulo_view(request, slug):
	last = Noticia.objects.order_by('-id')[:15]
	articulo = get_object_or_404(Noticia, slug=slug)	
	articulo.visto = articulo.visto + 1
	articulo.save()
	return render(request, 'noticia.html', {'noticia': articulo, 'lasts': last})

def seccion(request, seccion_n):
	last = Noticia.objects.order_by('-id')[:15]
	page = request.GET.get('page', 1)
	arts = Noticia.objects.filter(seccion__slug=seccion_n)
	seccion = seccion_n
	paginator = Paginator(arts, 9)
	try:
		articulos = paginator.page(page)
	except PageNotAnInteger:
		articulos = paginator.page(1)
	except EmptyPage:
		articulos = paginator.page(paginator.num_pages)
	return render(request, 'buscar_noticia.html', {'noticias': articulos, 'lasts': last, 'seccion': seccion})

def buscar(request):
	last = Noticia.objects.order_by('-id')[:15]
	q = request.GET.get('q', '')
	page = request.GET.get('page', 1)
	arts = Noticia.objects.filter(titulo__icontains=q)
	paginator = Paginator(arts, 9)
	try:
		articulos = paginator.page(page)
	except PageNotAnInteger:
		articulos = paginator.page(1)
	except EmptyPage:
		articulos = paginator.page(paginator.num_pages)

	data = None
	if len(q) > 0:
		data = '&q=' + q
	return render(request, 'buscar_noticia.html', {'noticias': articulos, 'lasts': last, 'data': data})

def blogs(request):
	last = Noticia.objects.order_by('-id')[:15]
	page = request.GET.get('page', 1)
	arts = Noticia.objects.order_by('-id')
	paginator = Paginator(arts, 9)
	try:
		articulos = paginator.page(page)
	except PageNotAnInteger:
		articulos = paginator.page(1)
	except EmptyPage:
		articulos = paginator.page(paginator.num_pages)
	return render(request, 'buscar_noticia.html', {'noticias': articulos, 'lasts': last})