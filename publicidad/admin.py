from django.contrib import admin

from .models import Banner

@admin.register(Banner)
class BannerAdmin(admin.ModelAdmin):
	list_display = ('nombre', 'telefono', 'url',)
	list_display_links = ('nombre', 'telefono', 'url',)