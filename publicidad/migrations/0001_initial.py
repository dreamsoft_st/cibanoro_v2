# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Banner',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nombre', models.CharField(max_length=100)),
                ('telefono', models.CharField(max_length=12, blank=True)),
                ('imagen', models.ImageField(upload_to=b'banners')),
                ('url', models.URLField(blank=True)),
                ('fecha_expira', models.DateField(null=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
