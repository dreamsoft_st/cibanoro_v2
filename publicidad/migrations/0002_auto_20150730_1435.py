# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('publicidad', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Fondo',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nombre', models.CharField(max_length=100)),
                ('imagen', models.ImageField(upload_to=b'banners')),
                ('activo', models.BooleanField(default=False)),
            ],
        ),
        migrations.AddField(
            model_name='banner',
            name='activo',
            field=models.BooleanField(default=False),
        ),
    ]
