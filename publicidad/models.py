from django.db import models

class Banner(models.Model):
	nombre = models.CharField(max_length=100)
	telefono = models.CharField(max_length=12, blank=True)
	imagen = models.ImageField(upload_to='banners')
	url = models.URLField(blank=True)
	fecha_expira = models.DateField(null=True)
	activo = models.BooleanField(default=False)

class Fondo(models.Model):
	nombre = models.CharField(max_length=100)
	imagen = models.ImageField(upload_to='banners')
	activo = models.BooleanField(default=False)
